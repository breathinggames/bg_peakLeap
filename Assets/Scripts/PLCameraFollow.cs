﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLCameraFollow : MonoBehaviour
{


    GameObject player;

    public float camVerticalOffset = 10;
    public float camFollowDistance = 10;

    void Start()
    {
        player = PLGameController.instance.player.gameObject;
    }

    void Update()
    {
        FollowPlayer();
    }

    // sets the cameras transform to follow behind the player, at vertical and distance offsets
    void FollowPlayer()
    {
        Vector3 camPos = new Vector3(player.transform.position.x, player.transform.position.y + camVerticalOffset, player.transform.position.z - camFollowDistance);
        transform.position = camPos;
    }

}
