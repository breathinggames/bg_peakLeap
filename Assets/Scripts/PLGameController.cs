﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLGameController : MonoBehaviour
{

    public static PLGameController instance;

    // the number of stars the player currently has collected
    public int starsCollected;

    public PLBallController player;

    [SerializeField]
    AudioSource source;

    int stagesComplete;

    void Awake()
    {
        instance = this;
        player = GameObject.Find("Player").GetComponent<PLBallController>();
    }


    // add to starsCollected and displays it in the mainUI
    public void IncreaseStars(int ammount)
    {
        starsCollected += ammount;
        PLMainUIController.instance.DisplayStars(starsCollected);
    }

    // called when the player hits an EndZone to initiate a reset, ready for the next zone
    public void StageComplete()
    {
        PLBallController.instance.ResetForNextStage();
        PLMainUIController.instance.StageCompletePanel();
    }

    // called when you hit the GameComplete zone, to end the game
    public void GameComplete()
    {
        PLMainUIController.instance.GameOverPanel();
        PLBallController.instance.Disable();
    }

    //called from other scripts to play a sound via the GameControllers audiosource
    public void PlaySound(AudioClip sound)
    {
        source.PlayOneShot(sound);
    }


}
