﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLPeakFlowTest : MonoBehaviour
{

    public static PLPeakFlowTest instance;

    // the maximum predicted volume the patient should be able to perform
    public float predictedVolume = 500;

    public float breathPower;

    public string peakFlowMessage1, peakFlowMessage2, peakFlowMessage3;

    void Awake()
    {
        instance = this;
    }

    // called from the gamecontroller when the player initiates a peak flow. repeatedly calls the method below, until cancelled
    public void PerformPeakFlow()
    {
        InvokeRepeating("ChargePeakFlow", 0, 0.01f);
    }

    // adds a set ammount to the breathPower to simulate a peak flow. When breath goes over certain percentages, displays messages of encouragement
    void ChargePeakFlow()
    {
        breathPower += 10f;
        if (ReturnPeakFlowPercentage() > 25 && ReturnPeakFlowPercentage() < 50)
        {
            PLMainUIController.instance.DisplayMessage(peakFlowMessage1);
        }
        if (ReturnPeakFlowPercentage() > 50 && ReturnPeakFlowPercentage() < 75)
        {
            PLMainUIController.instance.DisplayMessage(peakFlowMessage2);
        }

        if (ReturnPeakFlowPercentage() > 75)
        {
            PLMainUIController.instance.DisplayMessage(peakFlowMessage3);
        }
    }

    // when the mouse is released the simulated peak flow is over - the invoke to add to breathPower is cancelled and the ball gets launched
    public void FinishPeakFlow()
    {
        CancelInvoke("ChargePeakFlow");
        PLMainUIController.instance.ResetMessage();
    }

    // converts the breathPower into a percentage of the childs 'Predicted Volume' (which is currently just set at a fixed value)
    // this is important, as it means that the ball will always launch at a force relative to what the child is capable of doing.
    // eg: a bigger person should not be allowed to blow the ball higher than a smaller possibly sick person. everybody should 
    // feel rewarded for doing their best. Therefore the force of the launch is based on the percentage of their 'best effort',
    // rather than their actual output.
    public int ReturnPeakFlowPercentage()
    {
        return Mathf.RoundToInt((breathPower / predictedVolume) * 100);
    }

    public void ResetBreathPower()
    {
        breathPower = 0;
    }



}
