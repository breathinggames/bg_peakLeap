# Notes for Peak Flow Game
###<i>(March 1 - 3, 2017; Geneva, Switzerland; Lift Lab 2017)</i>

- existing peak flow game (team: Aurelien, Yannick, and Alex) designed in Montr�al
- adapted in Geneva at Lift Lab (team: Ned Birkin, Gareth Brown, Maria Frangos, Lucile Chabre, Lai-Tze Fan)


<hr />


## BACKGROUND INFO ON PEAK FLOW METER
- peak expiratory flow measures a person's maximum speed of expiration with a device called a peak flow meter
	- is able to measure level of obstruction in bronchial airways for people with asthma

- ideally, three hard breaths each time, two times a day, for at least 14 days in order to track any deviation in peak flow values
	- especially useful for doctors assessing changes in peak flow values before/after medical treatent or recommendations
	- not as useful for patients who have consistent, non-chronic peak flow values

- see these charts detailing the <a href="http://tvscn.nhs.uk/wp-content/uploads/2014/09/Paediatric-Normal-Values.pdf">normal pediatric peak flow values</a>
	- measured through factors including: height, age, sex, (and ethnicity)



## ORIGINAL OBJECTIVES (Montr�al)
- encourage children aged 6-12 to do a self-assessment of their peak flow by turning the peak flow meter into an element of an interactive video game
- track the changes in their peak flow with corresponding charts
- original design allowed a user to blow regularly into the peak flow device to move a ball along a path
	- objective was to blow very hard to make the ball smash through obstacles and continue its trajectory



## ADDITIONAL ADAPTED OBJECTIVES (Geneva)
- we spoke to Fran�ois, a respiratory physiotherapist, about the process of blowing into the peak flow device, to get a better idea of how the device could be used as part of a game
- the point is to blow into the device as hard as possible three separate times, so we did not see the point of having to blow regularly into the peak flow device--especially if this would put the patient out of breath and produce an inaccurate peak flow measurement
- we decided to change the design of the game so that each blow would correspond to one "turn" in the game
	- doing three peak flow measurements would allow a user to project the ball three times through an obstacle course


## Considerations in Design
- for incentive and reward, we added stars to the course that can be collected to be used in the My Room platform
- for incentive to do the peak flow measurement properly (blowing into the device as hard/fast as possible), we created three tiers/levels so that proper blows will project the ball higher, where there are more stars to collect


## To Do
- may add additional sound effects
- different obstacles courses to be added later (Lai-Tze has the designs)
- avatars to become personalized