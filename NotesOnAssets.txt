Meshes made in Blender: 

Floor tiles: 4x4x1
Wall tiles: 4x4x1

All FBX meshes exported at scale 1 in Blender, then scaled up in Unity's mesh-import settings at 100.

The Blender files are saved in the Project Folder in 'BlenderFiles', but the game uses .fbx files exported from Blender
